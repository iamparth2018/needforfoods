package com.nff.needforfoods;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements HomeAdapter.ItemListener {
    private RecyclerView recyclerView;
    private ArrayList<Item> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        arrayList = new ArrayList<>();

        arrayList.add(new Item("Category 1", R.mipmap.needforfoodsicon, "#BDBDBD"));
        arrayList.add(new Item("Category 2", R.mipmap.needforfoodsicon, "#BDBDBD"));
        arrayList.add(new Item("Category 3", R.mipmap.needforfoodsicon, "#BDBDBD"));
        arrayList.add(new Item("Category 4", R.mipmap.needforfoodsicon, "#BDBDBD"));
        arrayList.add(new Item("Category 5", R.mipmap.needforfoodsicon, "#BDBDBD"));
        arrayList.add(new Item("Category 6", R.mipmap.needforfoodsicon, "#BDBDBD"));
//        arrayList.add(new Item("Item 6", R.mipmap.needforfoodsicon, "#0A9B88"));

        HomeAdapter adapter = new HomeAdapter(MainActivity.this, arrayList, this);
        recyclerView.setAdapter(adapter);


        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
    }


    @Override
    public void onItemClick(Item item) {
        Toast.makeText(getApplicationContext(), item.text + " is clicked", Toast.LENGTH_SHORT).show();
    }
}
